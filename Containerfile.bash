#!/usr/bin/env bash

set -eux

image=localhost/motion
builder=$(buildah from docker.io/debian:bookworm-slim) || exit

buildah config -e DEBIAN_FRONTEND=noninteractive \
  -e DEBCONF_NONINTERACTIVE_SEEN=true "$builder"

buildah run "$builder" -- apt-get update -qqy
buildah run "$builder" -- apt-get install -qqy --option Dpkg::Options::="--force-confnew" --no-install-recommends \
  autoconf automake build-essential pkgconf libtool libzip-dev libjpeg-dev tzdata \
  git libavformat-dev libavcodec-dev libavutil-dev libswscale-dev libavdevice-dev \
  libwebp-dev gettext autopoint libmicrohttpd-dev ca-certificates imagemagick curl wget \
  libavformat-dev libavcodec-dev libavutil-dev libswscale-dev libavdevice-dev ffmpeg x264
buildah run "$builder" -- apt-get --quiet autoremove --yes
buildah run "$builder" -- apt-get --quiet --yes clean
buildah run "$builder" -- sh -c 'rm -rf /var/lib/apt/lists/*'
buildah run "$builder" -- git clone https://github.com/Motion-Project/motion.git /motion

buildah config --workingdir /motion "$builder"
buildah run "$builder" -- autoreconf -fiv
buildah run "$builder" -- ./configure
buildah run "$builder" -- sh -c 'make clean && make && make install'

motion=$(buildah from docker.io/debian:bookworm-slim)

config=(
  --entrypoint '["/usr/local/bin/motion", "-n"]'
  --port 8081
  --user 1001
  --author 'Stefan Midjich <swehack at gmail dot com>'
)

buildah run "$motion" -- apt-get update -qqy
buildah run "$motion" -- apt-get install -qqy --option Dpkg::Options::="--force-confnew" --no-install-recommends \
  tzdata libmicrohttpd12 ca-certificates imagemagick curl wget ffmpeg x264 awscli
buildah run "$motion" -- apt-get --quiet autoremove --yes
buildah run "$motion" -- apt-get --quiet --yes clean
buildah run "$motion" -- sh -c 'rm -rf /var/lib/apt/lists/*'
buildah copy --from "$builder" "$motion" '/usr/local' '/usr/local'

buildah config "${config[@]}" "$motion"
buildah commit "$motion" "$image"
