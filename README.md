# Motion container image

My personal container image of motion, used on various devices with cameras to detect motion.

# Build image

    bash Containerfile.bash

# Run image

    podman volume create motion-data
    podman run --rm --name motion --device /dev/video0 \
        -p 8081:8081 -v "$(pwd)/motion.conf:/usr/local/etc/motion/motion.conf:Z" \
        -v "motion-data:/usr/local/var/lib/motion:Z" \
        --group-add keep-groups --env-host registry.gitlab.com/stemid/motion-container:latest

## Environment

Motion runs event scripts that read their configuration from environment, here is an example.

```
export PUSHOVER_TOKEN=xxx
export PUSHOVER_USER=xxx
export PUSHOVER_URL=https://api.pushover.net/1/messages.json
export MOTION_URL=http://192.168.31.1:8081
```

# Notes

## Get supported resolutions of webcam

First get the bus and device ID.

```
$ lsusb | grep -i cam
Bus 001 Device 003: ID 13d3:5419 IMC Networks Integrated Camera
```

Then use that to get more verbose info.

```
$ sudo lsusb -s 001:003 -v|grep -Ei '(width|height)'
        wWidth                           1280
        wHeight                           720
```

## Modify motion.conf

The included motion.conf in this repo is for my setup, you can copy it and make the container use your own like this.

```
cp motion.conf motion.mine.conf
podman run --rm ... -v "$(pwd)/motion.mine.conf:/usr/local/etc/motion/motion.conf:Z"
```

## Add user to video group on immutable Fedora

For local testing on immutable Fedora first you need to copy the group from ``/usr/lib/group`` to ``/etc/group``.

    grep -E '^video' /usr/lib/group | sudo tee -a /etc/group

Then you can add yourself to it as normal, and it's safest to reboot to ensure that all processes understand the change.

    sudo usermod -aG video stemid
    sudo setsebool -P container_use_devices=on
    systemctl reboot

## How to deploy as Quadlet

Assuming you use a motion user account to run this quadlet.

### Add the motion user to the video group on Fedora or RHEL.

Logout or reboot to make that work.

    sudo usermod -a -G video motion

### Ensure containers can access devices in SElinux.

Our device is /dev/video0 in this case.

    sudo setsebool -P container_use_devices=on

### Create quadlet files

Create ``/home/motion/.config/containers/systemd/motion.volume`` with this content;

```
[Volume]
```

Create ``/home/motion/.config/containers/systemd/motion.container`` with this content;

```
[Unit]
Description=motion
Wants=network-online.target
After=network-online.target

[Container]
ContainerName=motion
Image=registry.gitlab.com/stemid/motion-container:latest
AutoUpdate=registry
EnvironmentFile=%h/.config/motion/environment
Volume=motion.volume:/var/lib/motion:Z
Volume=/home/motion/.config/motion/motion.conf:/usr/local/etc/motion/motion.conf:Z
Volume=/home/motion/.config/motion/on_event_start:/usr/local/etc/motion/on_event_start:Z
Volume=/home/motion/.config/motion/on_event_end:/usr/local/etc/motion/on_event_end:Z
PublishPort=8081:8081
AddDevice=/dev/video0
PodmanArgs=--group-add keep-groups

[Service]
Restart=always

[Install]
WantedBy=multi-user.target default.target
```

Create ``/home/motion/.config/motion/environment`` with something like this example;

```
PUSHOVER_TOKEN=xxxsecret
PUSHOVER_USER=xxxsecret
PUSHOVER_URL=https://api.pushover.net/1/messages.json
MOTION_URL=http://wireguard.host.to.access.camera:8081
AWS_ACCESS_KEY_ID=AKIAXXXSECRET
AWS_SECRET_ACCESS_KEY=xxxsecret
AWS_DEFAULT_REGION=eu-central-1
AWS_DEFAULT_S3_BUCKET=motion-bucket
```

### Event scripts

Install the scripts from ``on_event_start`` and ``on_event_end`` into ``/home/motion/.config/motion`` so the paths match with the Quadlet.

### Enable and run container

    systemctl --user daemon-reload
    systemctl --user enable --now motion
    podman ps -a
    podman logs -f motion

## Troubleshooting

If you still get permission denied errors for /dev/video0 try ``sudo setenforce 0`` and see if that helps.

If you get permission denied errors for storing images and videos then try stopping the service, deleting the container, deleting the motion volume, and re-starting everything. The motion volume will be re-created.
