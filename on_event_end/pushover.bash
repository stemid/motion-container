#!/usr/bin/env bash

set -x
LC_ALL=C

latest() {
  local file latest;
  for file in "${1:-.}"/*; do
    [[ "$file" -nt "$latest" ]] && latest="$file"
  done
  printf '%s\n' "$latest"
}

curl -sLX POST "${PUSHOVER_URL}" \
  --form-string "token=${PUSHOVER_TOKEN}" \
  --form-string "user=${PUSHOVER_USER}" \
  --form-string "title=Alert over" \
  --form-string "message=Movement no longer detected" \
  --form-string "priority=0" \
  --form-string "ttl=86400"

lastFile=$(latest /var/lib/motion)
ls -lath "$lastFile"
aws s3 cp "$lastFile" s3://$AWS_DEFAULT_S3_BUCKET/
aws s3 ls s3://$AWS_DEFAULT_S3_BUCKET/

find "${MOTION_TARGET_DIR:-/var/lib/motion}/" -type f -delete

# TODO: upload files to off-site storage
# TODO: purge target dir
# TODO: add url=https://where.images.are.uploaded
# TODO: add url_title=View images
