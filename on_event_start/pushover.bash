#!/usr/bin/env bash

set -x

curl -sLX POST "${PUSHOVER_URL}" \
  --form-string "token=${PUSHOVER_TOKEN}" \
  --form-string "user=${PUSHOVER_USER}" \
  --form-string "title=Movement!" \
  --form-string "message=Movement detected by laptop!" \
  --form-string "priority=1" \
  --form-string "ttl=86400" \
  --form-string "url=${MOTION_URL}" \
  --form-string "url_title=Video stream"

